# layerous_mods

Collection of my modding efforts, no matter how useless they turn out to be

## Rimworld

- [ ] LES Advanced Approximate Assemblies: a mod that allows you to research methods to fabricate products so similar to their original counterparts, that they will fool any outsider into thinking that youre selling them the real deal. Also they work exactly the same as them. Ready for testing (HMU on Steam to get access).
- [ ] LES Ingenious Industrial Implants: adds industrial-era implants which usually have some sort of a tradeoff (such as removing any chance of addiction BUT no longer will the pawn get (de)buffs from drugs/food). Not ready for testing as it is nowhere near finished.

## JKA

- [ ] A custom skin, based on [Chiss Reborn](https://jkhub.org/files/file/1112-chiss-reborn/?tab=comments)

## License

TODO: Choose one (prolly MIT or some such)

```
Check out the Releases page
```
