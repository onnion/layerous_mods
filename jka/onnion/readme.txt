*********************************** 
Jedi Knight: Jedi Academy 
*********************************** 
[BASE SKIN]
TITLE:  Chiss Reborn
AUTHOR: Darth Ayreon
E-MAIL: r_v_d_veen@hotmail.com
RELEASED: DATE: October 23, 2009

CREDITS: 
Raven, for the Reborn model.
Photoshop, for the tool.
[/BASE SKIN]

CREDITS:
Darth Ayreon for the Chiss Reborn skin.
Free Software Foundation for the tools used.
Triternion game studio for the foppish sound effects.
Disney corporation for playing us like a damn fiddle.

THIS MODIFICATION IS NOT MADE, DISTRIBUTED, OR SUPPORTED BY ACTIVISION, RAVEN, OR 
LUCASARTS ENTERTAINMENT COMPANY LLC. ELEMENTS TM & � LUCASARTS 
ENTERTAINMENT COMPANY LLC AND/OR ITS LICENSORS.
